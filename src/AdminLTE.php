<?php

namespace Lliure\Decorator\AdminLTE;

use Lliure\LliurePanel\Tools\Environment;
use Lliure\LliurePanel\Tools\TemplateEngineInterfece;
use Psr\Http\Message\ResponseInterface;

class AdminLTE
{
	
	public function __invoke(ResponseInterface $response): ResponseInterface{
		
		$twig = Environment::find($response);
		
		if($twig === false){
			return $response;
		}
		
		/** @var TemplateEngineInterfece $engine */
		$engine = $twig->getEngine();
		$engine::addPath(__DIR__ . '/Views', 'Template');
		
		return $response;
	}
	
}